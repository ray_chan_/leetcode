//
//  rotate_array.cpp
//  LeetCode
//
//  Created by Wen-Tung Chan on 8/3/15.
//  Copyright (c) 2015 Wen-Tung Chan. All rights reserved.
//
#include <vector>
using std::vector;

class RotateArray {
public:
  // method 1
  // if extra memory usage is allowed
  // insert take O(n)
  void rotate1(vector<int>& nums, int k) {
    size_t size = nums.size();
    if (size == 0)
      return;
    vector<int> result;
    k = k%size;
    result.insert(result.end(), nums.end() - k, nums.end());
    result.insert(result.end(), nums.begin(), nums.begin() + (size - k));
    nums = result;
  }
  
  // method 2
  // will have to shift elements every time
  // operation in loop take O(n)
  void rotate2(vector<int>& nums, int k) {
    size_t size = nums.size();
    if (size == 0)
      return;
    k = k%size;
    for(int i = 0; i < k; i++) {
      int temp = nums.back();
      nums.pop_back();
      nums.insert(nums.begin(), temp);
    }
  }
  
  // method 2-2
  // Shift only once
  // insert and erase take O(n)
  void rotate2_2(vector<int>& nums, int k) {
    size_t size = nums.size();
    if (size == 0)
      return;
    k = k%size;
    nums.insert(nums.begin(), nums.end() - k, nums.end());
    nums.erase(nums.end()-k, nums.end());
  }
  
  // method 3
  // reverse take half of size O(n)
  void rotate(vector<int>& nums, int k) {
    size_t size = nums.size();
    if (size == 0)
      return;
    k = k%size;
    std::reverse(nums.begin(), nums.end());
    std::reverse(nums.begin(), nums.begin() + k);
    std::reverse(nums.begin() + k, nums.end());
  }
};

/*
 int main() {
 RotateArray solution;
 vector<int> input{1,2,3,4,5,6,7};
 solution.rotate(input, 3);
 return 0;
 }
 */
