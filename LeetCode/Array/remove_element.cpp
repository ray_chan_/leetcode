//
//  remove_element.cpp
//  LeetCode
//
//  Created by Wen-Tung Chan on 8/3/15.
//  Copyright (c) 2015 Wen-Tung Chan. All rights reserved.
//

#include <vector>
using std::vector;
class RemoveElement {
public:
  int removeElement2(vector<int>& nums, int val) {
    if (nums.size() == 0) {
      return 0;
    }
    auto front = nums.begin();
    auto back = nums.end() - 1;
    while (back >= front && *back == val) {
      --back;
    }
    while (front <= back) {
      if (*front == val) {
        std::swap(*front, *back);
        --back;
      } else {
        ++front;
      }
    }
    int newLength = std::distance(nums.begin(), back) + 1;
    nums.erase(nums.begin() + newLength, nums.end());
    return newLength;
  }
  
  // best sulution
  // run time: O(n) to get new length
  // erase take size - index, O(n)
  int removeElement(vector<int>& nums, int val) {
    int index = 0;
    for (int i : nums) {
      if (i != val)
        nums[index++] = i;
    }
    nums.erase(nums.begin() + index, nums.end());
    return index;
  }
};