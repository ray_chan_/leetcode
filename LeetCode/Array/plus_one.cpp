//
//  PlusOne.cpp
//  LeetCode2
//
//  Created by Wen-Tung Chan on 8/1/15.
//  Copyright (c) 2015 Wen-Tung Chan. All rights reserved.
//
//https://leetcode.com/problems/plus-one/

#include <vector>
using std::vector;

class PlusOne {
 public:
  // iterate the array from behind just like how we do math
  // run time: O(n)
  // vector.insert take O(n)
  vector<int> plusOne(vector<int>& digits) {
    int carry = 1;
    for (int i = digits.size() - 1; i >= 0; --i) {
      int temp_digit = digits[i] + carry;
      digits[i] = temp_digit%10;
      carry = temp_digit/10;
      if (carry == 0) {
        return digits;
      }
    }
    if (carry > 0) {
      digits.insert(digits.begin(), carry);
    }
    return digits;
  }
};

/*
int main() {
  PlusOne solution;
  vector<int> digits(3,9);
  auto result = solution.plusOne(digits);
 
  return 0;
}
*/