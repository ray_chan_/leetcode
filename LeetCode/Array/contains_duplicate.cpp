//
//  ContainsDuplicate.cpp
//  LeetCode2
//
//  Created by Wen-Tung Chan on 8/1/15.
//  Copyright (c) 2015 Wen-Tung Chan. All rights reserved.
//
// https://leetcode.com/problems/contains-duplicate/

#include <vector>
#include <set>
using std::vector;
using std::set;
using std::sort;

class ContainsDuplicate {
 public:
  // method 1
  // sort the array O(nLogn)
  // go through every element to find duplicate O(n)
  // memory usage O(1) or none
  bool containsDuplicate(vector<int>& nums) {
    if (nums.size() <= 1) {
      return false;
    }
    sort(nums.begin(), nums.end());
    for (int i = 0; i < nums.size() - 1; ++i) {
      if (nums[i+1] == nums[i]) {
        return true;
      }
    }
    return false;
  }
    
  // method 2
  // use set to store present value
  // run time O(n)
  // set.count and set.insert take O(Logn)
  // memory usege O(n)
  bool containsDuplicate2(vector<int>& nums) {
    set<int> present;
    for (auto value : nums) {
      if (present.count(value)) {
        return true;
      } else {
        present.insert(value);
      }
    }
    return false;
  }
};
