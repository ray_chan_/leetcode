//
//  main.cpp
//  LeetCode2
//
//  Created by Wen-Tung Chan on 8/1/15.
//  Copyright (c) 2015 Wen-Tung Chan. All rights reserved.
//

#include <iostream>
#include "Array/remove_element.cpp"
using std::cout;
using std::endl;

int main(int argc, const char * argv[]) {
  
  vector<int> input{4, 5};
  RemoveElement solution;
  int newLength = solution.removeElement(input, 4);
  return 0;
}
