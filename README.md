# README #

This is a repository that I use to track my LeetCode solutions.
The solutions are grouped by tags on LeetCode
https://leetcode.com/

The coding style follows Google c++ coding style.
http://google-styleguide.googlecode.com/svn/trunk/cppguide.html

Here are some exceptions:
* The header file of the solutions are omitted to keep it simple.
* The solution function name match the Leetcode naming, which start with lowercase.